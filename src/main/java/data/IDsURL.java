package data;

import org.testng.annotations.DataProvider;

public class IDsURL extends Base {
	
	@DataProvider
	public Object[][] login() {
		Object[][] data=new Object[20][2];
				
		data [1][0]="199106212393";
		data [1][1]="https://avonova-no.app.dev.platform24.se";
				
	    data [2][0]="199106212393";
		data [2][1]="https://capio-tyresostrand.app.staging.platform24.se/home-login";
				
	    data [4][0]="199106212393";
		data [4][1]="https://cereb.app.dev.platform24.se/home-login";
				
		data [5][0]="196412277169";
		data [5][1]="https://euroaccident.app.dev.platform24.se/";
				
		data [6][0]="199106212393";
		data [6][1]="https://familjelakarnamitt-fransta.app.dev.platform24.se/";
						
		data [7][0]="199106212393";
		data [7][1]="https://gavleborg-dvm.app.dev.platform24.se";
		
		data [7][0]="199106212393";
		data [7][1]="https://doktor24.app.dev.platform24.se";
		
		data [8][0]="199106212393";
		data [8][1]="https://feelgood.app.dev.platform24.se";
		
		data [9][0]="199106212393";
		data [9][1]="https://if-se.app.dev.platform24.se/home-login";
		
		data [10][0]="197412083532";
		data [10][1]="https://lansforsakringar.app.demo.platform24.se";
		
		data [11][0]="199106212393";
		data [11][1]="https://regionjh-lit.app.dev.platform24.se";
		
		data [12][0]="199106212393";
		data [12][1]="https://regionnorrbotten-digitalen.app.demo.platform24.se";
		
		data [13][0]="199106212393";
		data [13][1]="https://regionskane-pvo.app.dev.platform24.se";
		
		data [14][0]="199106212393";
		data [14][1]="https://regionvasterbotten-halsodigitalen.app.dev.platform24.se/home-login";
		
		data [16][0]="199106212393";
		data [16][1]="https://regionvastmanland-1177.app.dev.platform24.se/";

		data [17][0]="196705149281";
		data [17][1]="https://seb.app.dev.platform24.se/home-login";

		data [18][0]="199106212393";
		data [18][1]="https://skandia.app.dev.platform24.se/home-login";
		
		data [19][0]="199106212393";
		data [19][1]="https://stuvstavc.app.dev.platform24.se";
		
		//aleris-NO and Region Sörmland is missing
		return data;	
}
	
	@DataProvider
	public Object[][] smoke() {
		Object[][] data=new Object[7][2];
				
				
	    data [0][0]="199106212393";
		data [0][1]="https://capio-tyresostrand.app.staging.platform24.se/home-login";
		
		data [1][0]="199106212393";
		data [1][1]="https://if-se.app.dev.platform24.se/home-login";
		
		data [2][0]="199106212393";
		data [2][1]="https://regionjh-lit.app.dev.platform24.se";
		
		data [3][0]="199106212393";
		data [3][1]="https://regionnorrbotten-digitalen.app.demo.platform24.se";
		
		data [4][0]="196705149281";
		data [4][1]="https://seb.app.dev.platform24.se/home-login";

		data [5][0]="199106212393";
		data [5][1]="https://skandia.app.dev.platform24.se/home-login";
		
		data [6][0]="199106212393";
		data [6][1]="https://stuvstavc.app.dev.platform24.se";
		
		return data;
	}
	@DataProvider
	public Object[][] pickOrigin() {
		Object[][] data=new Object[7][1];
				
		data [0][0]="https://gavleborg.app.dev.platform24.se/pick-origin";
		data [1][0]="https://regionsormland.app.dev.platform24.se/pick-origin";
		data [2][0]="https://regionjh.app.dev.platform24.se/pick-origin";
		data [3][0]="https://regionnorrbotten-digitalen.app.dev.platform24.se/pick-origin";
		data [4][0]="https://familjelakarnamitt.app.staging.platform24.se/pick-origin";
		data [5][0]="https://regionvastmanland.app.dev.platform24.se/pick-origin";
		data [6][0]="https://capio-dk.app.dev.platform24.se/pick-origin";	
		return data;}

	@DataProvider
	public Object[][] InsuranceSSN ()
		{
			Object[][] data=new Object[1][3];
			data [0][0]="195811123073";
			data [0][1]="babyyoda@platform24.se";
			data [0][2]="199805122380";
			return data; }

	@DataProvider
	public Object[][] GeneralSSN()
	{ Object[][] data=new Object[1][3];
		data [0][0]="199106212393";
		data [0][1]="babyyoda@platform24.se";
		data [0][2]="+4671234567800";
		//"192810098372"
		return data; }

	@DataProvider
	public Object[][] smokeRegions() {
		Object[][] data=new Object[7][2];
		data [0][0]="199106212393";
		data [0][1]="https://capio-tyresostrand.app.staging.platform24.se/home-login";
		data [1][0]="199106212393";
		data [1][1]="https://regionvastmanland-1177.app.dev.platform24.se/home-login";
		data [2][0]="199106212393";
		data [2][1]="https://regionjh-lit.app.dev.platform24.se";
		data [3][0]="199106212393";
		data [3][1]="https://regionnorrbotten-digitalen.app.demo.platform24.se";
		data [4][0]="196705149281";
		data [4][1]="https://regionvasternorrland-1177vardchatten.app.dev.platform24.se/home-login";
		data [5][0]="199106212393";
		data [5][1]="https://skandia.app.dev.platform24.se/home-login";
		data [6][0]="199106212393";
		data [6][1]="https://regionhalland-backagardvc.app.dev.platform24.se/";
		return data;
	}
}
