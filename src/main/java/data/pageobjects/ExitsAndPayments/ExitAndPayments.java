
package data.pageobjects.ExitsAndPayments;

import data.pageobjects.Triage.Triages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

public class ExitAndPayments extends Triages {

	public ExitAndPayments(WebDriver driver)
	{super(driver); }

	private By RecommendationSeekCareOnlineButton= By.xpath("//button[contains(@data-testid,'seekCareOnline')]");
	private By AcceptPaymentOfflineButton= By.cssSelector("button[role='button']");
	private By ConfirmContactDetailsButton= By.cssSelector("button[type='submit']");
	private By PickFirstTimeSlotAvailable= By.cssSelector("section[aria-label='Välj en tid som passar dig'] fieldset button");
	private By ChooseDropIn= By.cssSelector("button[data-testid='exitActionsModalButton-Träffa_läkare_online']");
	private By ChooseToBook= By.cssSelector("button[data-testid='exitActionsModalButton-Boka_tid']");
	private By GoToKlarnaPayment= By.cssSelector("button[role='button']");
	private By Freepass= By.cssSelector("li button");
	private By selectFreePass= By.cssSelector("div[role='presentation'] button");
	private By AssertDropInQueue= By.cssSelector("div[data-testid='info-container']");
	private By AssertConfirmBooking= By.xpath("//button[contains(@class,'BookingConfirm')]");
	private final By ElevenSeventySevenBtn= By.cssSelector("[data-testid='button-option-call1177']");
	

	public WebElement RecommendationSeekCareOnlineButton() {
		return waitMedium.until(ExpectedConditions.elementToBeClickable(RecommendationSeekCareOnlineButton));}
	public WebElement AcceptPaymentOfflineButton() {return waitMedium.until(ExpectedConditions.elementToBeClickable(AcceptPaymentOfflineButton));
}	public Boolean waitUntilURLisOfflinePayment() {return waitMedium.until(ExpectedConditions.urlContains("/paymentOffline"));}
	public WebElement ConfirmContactDetailsButton() { return waitMedium.until(ExpectedConditions.elementToBeClickable(ConfirmContactDetailsButton));}
	public WebElement PickFirstTimeSlotAvailable() { return waitMedium.until(ExpectedConditions.elementToBeClickable(PickFirstTimeSlotAvailable));}
	public WebElement ChooseDropIn() {
		return waitMedium.until(ExpectedConditions.elementToBeClickable(ChooseDropIn));
}
	public WebElement ChooseToBook() {
		return waitMedium.until(ExpectedConditions.elementToBeClickable(ChooseToBook));
}
	public WebElement GoToKlarnaPayment() {return waitMedium.until(ExpectedConditions.elementToBeClickable(GoToKlarnaPayment));}
	public ExitAndPayments selectIhaveAFreePass() {
	 waitMedium.until(ExpectedConditions.elementToBeClickable(Freepass)).click();
	 waitMedium.until(ExpectedConditions.elementToBeClickable(selectFreePass)).click();
	 return this;}
	public int assertDropInQueue() { int size =waitMedium.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(AssertDropInQueue)).size();
	 return size;}
	public int assertConfirmBooking() { int size =waitMedium.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(AssertConfirmBooking)).size();
		return size;}
	public int contactElevenSeventySevenBtn() {
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(1));
		int btn= driver.findElements(ElevenSeventySevenBtn).size();
		return btn;  }
	public WebElement contactElevenSeventySevenButton() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(ElevenSeventySevenBtn));}

	/*	public void GoThroughKlarnaPayment( ) throws InterruptedException {
		wait= new WebDriverWait(driver, Duration.ofSeconds(5));
		ExitAndPayments choose= new ExitAndPayments(driver);
		Thread.sleep(3000);
		wait.until(ExpectedConditions.visibilityOfElementLocated(GoToKlarnaPayment));
		choose.GoToKlarnaPayment().click();
		//choose.ConfirmContactDetailsButton().click(); I commented out because it is not asked everytime
		System.out.println(driver.findElements(By.tagName("iframe")).size());
		driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[id='klarna-checkout-iframe']")));
		driver.findElement(By.id("billing-email")).sendKeys("youremail@email.com");
		driver.findElement(By.id("billing-postal_code")).sendKeys("12345");
		for(int i=0;i<3;i++) {

			Thread.sleep(1000);
			driver.findElement(By.id("button-primary")).click();

		Thread.sleep(1000);
		driver.findElement(By.id("button-primary")).click();
		}
		driver.findElement(By.cssSelector("button[data-cid='button.buy_button']")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[data-cid='button.buy_button']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-cid='button.buy_button']")));
		driver.findElement(By.cssSelector("button[data-cid='button.buy_button']")).click();
		//Payments stopped working from here, it can't find elements, tried to switch to another iframe but still does not work.
		driver.switchTo().frame(driver.findElement(By.cssSelector("iframe[id='klarna-fullscreen-iframe']")));
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("-f277b5c5-5161-4d6a-85bc-59b0a8629c07-klarna_category.invoice.-1__radio-mark")));
		wait.until(ExpectedConditions.elementToBeClickable(By.id("-f277b5c5-5161-4d6a-85bc-59b0a8629c07-klarna_category.invoice.-1__radio-mark")));
		driver.findElement(By.id("-f277b5c5-5161-4d6a-85bc-59b0a8629c07-klarna_category.invoice.-1__radio-mark")).click();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[data-cid='button.buy_button']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-cid='button.buy_button']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[data-cid='button.buy_button']")));

		driver.findElement(By.cssSelector("button[data-cid='button.buy_button']")).click();
		driver.switchTo().defaultContent();
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("button[role='button']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("button[role='button']")));
		driver.findElement(By.cssSelector("button[role='button']")).click();

	}*/
}
