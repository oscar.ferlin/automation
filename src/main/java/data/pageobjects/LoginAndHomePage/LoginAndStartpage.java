package data.pageobjects.LoginAndHomePage;

import data.pageobjects.Triage.Triages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;


 public class LoginAndStartpage extends Triages {


	public LoginAndStartpage(WebDriver driver) {
		super(driver);
// TODO Auto-generated constructor stub

}
	//Login
	private final By LoginButton= By.xpath("//button[@data-testid='login-button']");
	private final By InputFieldForLogin= By.cssSelector("input[data-testid='personal-number-input']");
	private final By TestLoginButton= By.cssSelector("button[data-testid='test-login-button']");
	//Start page titles
	private final By DigitalenTitle= By.cssSelector(".typography__Head2-patient-ui__axy8uk-1.iBWric.sc-htJRVC.lbuFOQ");
	//Start page items
	private final By EnterSeekCare= By.xpath("//div[@class='Homestyles__HomeContent-start-page__sc-8wiea9-6 jKoaua']//div[1]//button");
	private final By SearchForCondition= By.cssSelector("input[data-testid='search-input']");
	private final By EnterCondition= By.cssSelector("div[data-testid='search-results-container'] button");
	private final By PrescriptionRenewal= By.xpath("//div[@class='Homestyles__HomeContent-start-page__sc-8wiea9-6 jKoaua']//div[3]//button");

	//old askForPatient model
	private final By SeekCareForMyself= By.cssSelector("button[data-testid='adult']");
	private final By SeekCareForChild=By.cssSelector("button[data-testid='child']");

	private final By EnterSpringWormTriage= By.xpath("//button[text()='Springmask']");
	private final By myProfile= By.cssSelector("a[href='/aboutyou']");
	private final  By enterMyProfileContactDetails= By.cssSelector("a[href='/contactDetails?closable=true']");
	private final By contactDetailsMobileNumberInput= By.cssSelector("input[name='mobileNumber']");
	public  By contactDetailsEmailInput= By.cssSelector("input[name='email']");
	private final By ConfirmContactDetailsButton= By.cssSelector("button[type='submit']");
	private final By pickOriginLinks= By.tagName("a");
	//Doktor24 specific
	private final By AcceptCookiesDokto24= By.id("onetrust-accept-btn-handler");
	private final By LoginDoktor24=By.cssSelector("a[data-testid=home-login-button");




	public WebElement LoginButton() {
	return waitMedium.until(ExpectedConditions.elementToBeClickable(LoginButton));
}
	public WebElement InputFieldForLogin() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(InputFieldForLogin));}
	public WebElement TestLoginButton() { return waitMedium.until(ExpectedConditions.elementToBeClickable(TestLoginButton));}
	public WebElement DigitalenTitle() {
	return driver.findElement(DigitalenTitle);
}
	public LoginAndStartpage enterSeekCare() { waitLong.until(ExpectedConditions.visibilityOfElementLocated(EnterSeekCare)).click(); return this;}
	public WebElement searchForCondition() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(SearchForCondition));}
	public List <WebElement> enterCondition() { return waitMedium.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(EnterCondition));}
	public WebElement seekCareForMyself() {return waitLong.until(ExpectedConditions.visibilityOfElementLocated(SeekCareForMyself));}
	 public WebElement seekCareForChild() {return waitLong.until(ExpectedConditions.visibilityOfElementLocated(SeekCareForChild));}

	public WebElement AcceptCookiesDokto24() { return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(AcceptCookiesDokto24));}
	public WebElement LoginDoktor24() {
		return waitMedium.until(ExpectedConditions.elementToBeClickable(LoginDoktor24));
}
	public LoginAndStartpage enterSpringWormTriage() { waitMedium.until(ExpectedConditions.visibilityOfElementLocated(EnterSpringWormTriage)).click(); return this;}
	public WebElement myProfile() { return waitMedium.until(ExpectedConditions.elementToBeClickable(myProfile));}
	public WebElement enterMyProfileContactDetails() {return waitMedium.until(ExpectedConditions.elementToBeClickable(enterMyProfileContactDetails));}
	public WebElement contactDetailsMobileNumberInput() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated((contactDetailsMobileNumberInput)));}
	public WebElement contactDetailsEmailInput() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(contactDetailsEmailInput));}
	public WebElement ConfirmContactDetailsButton() {return waitMedium.until(ExpectedConditions.elementToBeClickable(ConfirmContactDetailsButton));}
	public WebElement PrescriptionRenewal() { return waitMedium.until(ExpectedConditions.elementToBeClickable(PrescriptionRenewal));}
	public List<WebElement> pickOriginLinks() {return driver.findElements(pickOriginLinks);}

	public LoginAndStartpage loginWithBankID(String ID) {
		LoginButton().click();
		InputFieldForLogin().sendKeys(ID);
		TestLoginButton().click();
		return this; }

	 public LoginAndStartpage enterPrescriptionRenewal() {
		PrescriptionRenewal().click();
		seekCareForMyself().click();
		seekCareForMyself().click();
		return this; }

	 public void enterTheCondition(String condition){
		searchForCondition().sendKeys(condition);
		List<WebElement> conditions = enterCondition();
		for(WebElement option:conditions) {
			if(option.getText().contains(condition)) {
				option.click();
				break; } } }

	public CancelAppointments getCancelAppointments() {return new CancelAppointments(driver);}


}
