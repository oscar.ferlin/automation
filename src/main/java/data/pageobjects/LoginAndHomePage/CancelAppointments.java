package data.pageobjects.LoginAndHomePage;

import data.pageobjects.Triage.Triages;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.time.Duration;

public class CancelAppointments extends Triages {

    public CancelAppointments(WebDriver driver) {
        super(driver);
    }

    private final By LeaveDropInQueue= By.cssSelector("a[href='/history']");
    private final By UpcomingAppointmentPopUp= By.cssSelector("[data-testid='upcomingAppointmentModalV3']");
    private final By SeeDetailsV3= By.cssSelector("[data-testid='upcomingAppointmentModalV3'] [role='button']");
    private final By CancelAppointment= By.cssSelector("button[role='link']");
    private final By YesIWantToCancel= By.xpath("//div[contains(@class,'TwoOptionsModal_main')]//button[1]");
    private final By backToStartPage= By.cssSelector("article button[role='button']");

    public int upcomingAppointmentPopUp() {
        //waitMedium.until(ExpectedConditions.urlContains("/home"));
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(3));
        int popup= driver.findElements(UpcomingAppointmentPopUp).size();
        return popup;  }

    public CancelAppointments clickOnSeeDetails() {
        waitMedium.until(ExpectedConditions.elementToBeClickable(SeeDetailsV3)).click();return this; }
    public CancelAppointments cancelTheAppointment() {
        waitMedium.until(ExpectedConditions.visibilityOfElementLocated( SeeDetailsV3)).click();
        waitMedium.until(ExpectedConditions.elementToBeClickable(CancelAppointment)).click();
        waitMedium.until(ExpectedConditions.elementToBeClickable(YesIWantToCancel)).click();
        waitMedium.until(ExpectedConditions.urlContains("/cancelConfirmation"));
        waitMedium.until(ExpectedConditions.elementToBeClickable(backToStartPage)).click();
        return this;  }
    public CancelAppointments leaveDropInQueue() {
        waitMedium.until(ExpectedConditions.elementToBeClickable(LeaveDropInQueue)).click(); return this;  }

}
