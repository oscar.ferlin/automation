package data.pageobjects.Triage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class WristInjury extends Triages {

    public WristInjury(WebDriver driver) {
        super(driver); }

    private final By EightSeverePain= By.cssSelector("label:nth-child(9)");
    public WebElement eightSeverePain() {return waitMedium.until(ExpectedConditions.elementToBeClickable(EightSeverePain));}


      public WristInjury wristInjuryPrio1() {
        Ok().click();
        eightSeverePain().click();
        Send().click();
        Yes().click();
        Yes().click();
        submitAnswer().click();
        return this;  }
}
