package data.pageobjects.Triage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class PrescriptionRenewals extends Triages {


    public PrescriptionRenewals(WebDriver driver) {
        super(driver);
        }

   private final By otherMedicine= By.cssSelector("li:nth-child(3)");
   private final By inputField= By.cssSelector("input[data-testid='search-input']");
   private final By prescriptionResults= By.cssSelector("div[data-testid='MedicationSearch__results'] li:nth-child(1)");
   private final By howManyCapsuleADay= By.cssSelector("input[data-testid='input__input']");
   private final By readyButton= By.xpath("(//button[@role='button'])[10]");
   private final By renewPrescriptionButton= By.xpath("(//button[@role='button'])[1]");
   private final By TemporaryWaitToSolveSyncIssue= By.cssSelector(".Text_text___HNk");
   private final By FinishInterviewNo= By.cssSelector("li:nth-child(2) label");
   private final By FinishInterviewWhenNeeded= By.cssSelector("li:nth-child(2)");
   private final By FinishInterviewMonths= By.xpath("//li");


   public WebElement otherMedicine() {return waitMedium.until(ExpectedConditions.elementToBeClickable(otherMedicine));}
   public WebElement renewPrescriptionButton() {return waitMedium.until(ExpectedConditions.elementToBeClickable(renewPrescriptionButton));}
   public WebElement inputField() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(inputField));}
   public WebElement prescriptionResults() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(prescriptionResults));}
   public WebElement howManyCapsuleADay() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(howManyCapsuleADay));}
   public WebElement readyButton() {return waitMedium.until(ExpectedConditions.elementToBeClickable(readyButton));}

   public WebElement finishInterviewWhenNeeded() {return waitMedium.until(ExpectedConditions.elementToBeClickable(FinishInterviewWhenNeeded));}
   public WebElement finishInterviewNo() { return waitMedium.until(ExpectedConditions.elementToBeClickable(FinishInterviewNo));}
   public WebElement finishInterviewMonths() { return waitMedium.until(ExpectedConditions.elementToBeClickable(FinishInterviewMonths));}
    //Erase when ID's exist
    public PrescriptionRenewals temporaryWaitToSolveSyncIssue() {
        waitMedium.until(ExpectedConditions.visibilityOfElementLocated(TemporaryWaitToSolveSyncIssue));
        Next().click();
        return this;}



    public PrescriptionRenewals navigateThroughAdultTriage() {
        PrescriptionRenewals answer = new PrescriptionRenewals(driver);
        otherMedicine().click();
        Next().click();
        inputField().sendKeys("alvedon");
        prescriptionResults().click();
        howManyCapsuleADay().sendKeys("5");
        readyButton().click();
        renewPrescriptionButton().click();
        temporaryWaitToSolveSyncIssue().
        FreeTypeInput().sendKeys("BlueBerry");
        Next().click();
        return this;
    }

    public PrescriptionRenewals navigateThroughPostTriageFinishInterview() throws InterruptedException {
       Actions action = new Actions(driver);
       FreeTypeInput().sendKeys("Hello World");
       Next().click();
       Thread.sleep(3000);
       finishInterviewNo().click();
       Next().click();
       Thread.sleep(3000);
       action.moveToElement(finishInterviewWhenNeeded()).click().build().perform();
       Next().click();
       Thread.sleep(3000);
       action.moveToElement(finishInterviewMonths()).click().build().perform();
       Next().click();
       Thread.sleep(3000);
       action.moveToElement(finishInterviewNo()).click().build().perform();
       Next().click();
       return this;
    }

}
