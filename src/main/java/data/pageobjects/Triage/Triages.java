package data.pageobjects.Triage;

import data.pageobjects.ExitsAndPayments.ExitAndPayments;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Triages {
    protected WebDriver driver;
    protected WebDriverWait waitShort;
    protected WebDriverWait waitMedium;
    protected WebDriverWait waitLong;

    public Triages(WebDriver driver) {
        this.driver=driver;
        waitShort= new WebDriverWait(driver, Duration.ofSeconds(3));
        waitLong= new WebDriverWait(driver, Duration.ofSeconds(20));
        waitMedium= new WebDriverWait(driver, Duration.ofSeconds(8)); }


    private final By No= By.cssSelector("li:nth-child(2) button:nth-child(1)");
    private final By Yes= By.cssSelector("li:nth-child(1) button:nth-child(1)");
    private final By Next= By.xpath("//button[@role='button']");
    private final By FreeTypeInput= By.cssSelector("input[data-testid='question-free-type-input']");
    private final By Ok= By.cssSelector("button[data-testid='continueChatButton']");
    private final By Send= By.cssSelector("button[data-testid='sendButton']");
    private final By submitAnswer= By.cssSelector("button[data-testid='endButton']");

    public WebElement Yes() {
        return waitMedium.until(ExpectedConditions.elementToBeClickable(Yes));
    }
    public WebElement No() {
        return waitMedium.until(ExpectedConditions.elementToBeClickable(No));
    }
    public WebElement Send() {
        return waitMedium.until(ExpectedConditions.elementToBeClickable(Send));
    }
    public WebElement Next() {return waitMedium.until(ExpectedConditions.elementToBeClickable(Next));}
    public WebElement FreeTypeInput() {return waitMedium.until(ExpectedConditions.visibilityOfElementLocated(FreeTypeInput));}
    public WebElement Ok() {
        return waitMedium.until(ExpectedConditions.elementToBeClickable(Ok));
    }
    public WebElement submitAnswer() { return waitMedium.until(ExpectedConditions.elementToBeClickable(submitAnswer));}

    public SpringWorm getSpringWormTriage() {return new SpringWorm(driver);}
    public PrescriptionRenewals getPrescriptionRenewalTriage() {return new PrescriptionRenewals(driver);}
    public ExitAndPayments getExitAndPayments() { return new ExitAndPayments(driver);}
    public WristInjury getWristInjuryTriage() {return new WristInjury(driver);}



}
