package data.pageobjects.Triage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class SpringWorm extends Triages {

        public SpringWorm(WebDriver driver) {
            super(driver); }


        private final By inTheAnus= By.xpath("//label[3]");
        private final By noneOfTheAbove= By.xpath("//li[4]");
        private final By otherDescription= By.cssSelector("li:nth-child(3) button:nth-child(1)");
        //post triage /Only for FinishInterviewer
        private final By finishInterviewNo= By.cssSelector("li:nth-child(2)");
        private final By finishInterviewNext= By.cssSelector("button[role='button']");
        private final By finishInterviewFreeTypeInput= By.cssSelector("input[data-testid='question-free-type-input']");
        private final By finishInterviewDiagnosis= By.cssSelector("li:nth-child(1)");
        private final By finishInterviewYes= By.cssSelector("li:nth-child(1)");


        public WebElement inTheAnus() {
            return waitMedium.until(ExpectedConditions.elementToBeClickable(inTheAnus));
        }
        public WebElement noneOfTheAbove() { return waitMedium.until(ExpectedConditions.elementToBeClickable(noneOfTheAbove));}
        public WebElement otherDescription() { return waitMedium.until(ExpectedConditions.elementToBeClickable(otherDescription));}
        //Post triage finish interviewer
        public WebElement finishInterviewNo() { return waitMedium.until(ExpectedConditions.elementToBeClickable(finishInterviewNo));}
        public WebElement finishInterviewNext() { return waitMedium.until(ExpectedConditions.elementToBeClickable(finishInterviewNext));}
        public WebElement finishInterviewFreeTypeInput() {return waitMedium.until(ExpectedConditions.elementToBeClickable(finishInterviewFreeTypeInput));}
        public WebElement finishInterviewDiagnosis() { return waitMedium.until(ExpectedConditions.elementToBeClickable(finishInterviewDiagnosis));}
        public WebElement finishInterviewYes() { return waitMedium.until(ExpectedConditions.elementToBeClickable(finishInterviewYes));}


        public void SpringWormPrio4() {
            SpringWorm answer =new SpringWorm(driver);
            answer.Ok().click();
            answer.No().click();
            answer.No().click();
            answer.inTheAnus().click();
            answer.Send().click();
            answer.noneOfTheAbove().click();
            answer.Send().click();
            answer.No().click();
            answer.otherDescription().click();
            answer.submitAnswer().click();

        }
        public void postTriageFinishInterviewWorm() throws InterruptedException {
            SpringWorm answer =new SpringWorm(driver);
            answer.finishInterviewNo().click();
            answer.finishInterviewNext().click();
            answer.finishInterviewFreeTypeInput().sendKeys("Blueberry soup is healthy");
            Thread.sleep(1000);
            answer.finishInterviewNext().click();
            answer.finishInterviewNo().click();
            Thread.sleep(1000);
            answer.finishInterviewNext().click();
            Thread.sleep(1000);
            answer.finishInterviewDiagnosis().click();
            answer.finishInterviewNext().click();
            Thread.sleep(1000);
            answer.finishInterviewNo().click();
            answer.finishInterviewNext().click();

        }
        public void postTriageWorm() throws InterruptedException {
            SpringWorm answer =new SpringWorm(driver);
            answer.No().click();
            Thread.sleep(1000);
            answer.finishInterviewNext().click();
            answer.No().click();
            answer.finishInterviewDiagnosis().click();
            Thread.sleep(1000);
            answer.finishInterviewNext().click();
            answer.No().click();
        }

    }


