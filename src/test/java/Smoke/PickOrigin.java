package Smoke;

import data.Base;
import data.IDsURL;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.List;

public class PickOrigin extends Base {
	public WebDriver driver;
	
	@BeforeTest
	public void reports() throws IOException{
		driver = initializeDriver();}

	@Test(dataProvider="pickOrigin", dataProviderClass=IDsURL.class)
	public void pickOrigin(String URL) throws MalformedURLException, IOException {
		LoginAndStartpage getAll= new LoginAndStartpage(driver);
		SoftAssert a= new SoftAssert();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
		driver.get(URL);
		List<WebElement> links=getAll.pickOriginLinks();
		for (WebElement link:links) {
			String url=link.getAttribute("href");
			HttpURLConnection connection=(HttpURLConnection) new URL(url).openConnection();
			connection.setRequestMethod("HEAD");
			connection.connect();
			int responseCode=connection.getResponseCode();
			System.out.println(responseCode);
			a.assertTrue(responseCode<400, "This origin" +link.getText()+" is broken with code"+responseCode);
		}
		driver.findElement(By.tagName("a")).click();
		int loginbutton= driver.findElements(By.cssSelector("button[data-testid='login-button']")).size();
		a.assertEquals(loginbutton, 1, "No Login button was found, check opening hours of the origin and compare to the test exection time");
		a.assertAll();
	}
	 @AfterTest
	 public void closeBrowser() {
		 driver.close();
	 }
}
