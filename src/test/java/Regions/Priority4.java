package Regions;

import data.Base;
import data.IDsURL;
import data.pageobjects.ExitsAndPayments.ExitAndPayments;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;
import data.pageobjects.Triage.Triages;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class Priority4 extends Base {
    public WebDriver driver;
    @BeforeTest
    public void driver() throws IOException {
        driver =initializeDriver();
    }

    @Test(dataProvider="GeneralSSN", dataProviderClass= IDsURL.class)
    public void regionPrio4(String ID, String Email, String Phone) throws InterruptedException {
        LoginAndStartpage user = new LoginAndStartpage(driver);
        ExitAndPayments exit= new ExitAndPayments(driver);
        Triages triage= new Triages(driver);

        driver.get(prop.getProperty("Vasternorrland"));
        user.loginWithBankID(ID);
        if(driver.getCurrentUrl().contains("/contactDetails")) {
            user.contactDetailsMobileNumberInput().sendKeys(Phone);
            user.contactDetailsEmailInput().sendKeys(Email);
            user.ConfirmContactDetailsButton().click();
        }
        if(user.getCancelAppointments().upcomingAppointmentPopUp()==1){
            user.getCancelAppointments().cancelTheAppointment();
        }
        user.enterSeekCare().enterTheCondition("springmask");
        user.seekCareForMyself().click();
        triage.getSpringWormTriage().SpringWormPrio4();
        if(exit.contactElevenSeventySevenBtn()==1) {
            Assert.assertTrue(driver.getCurrentUrl().contains("closable=true"));
        }
       else {
            exit.RecommendationSeekCareOnlineButton().click();
            int contactForm= driver.findElements(user.contactDetailsEmailInput).size();
            if(contactForm==1){
                user.ConfirmContactDetailsButton().click();
                exit.waitUntilURLisOfflinePayment();
                exit.AcceptPaymentOfflineButton().click();
            }
            else {
                exit.waitUntilURLisOfflinePayment();
                exit.AcceptPaymentOfflineButton().click();
                exit.ConfirmContactDetailsButton().click(); }
            if(driver.getCurrentUrl().contains("/finishinterview")) {
                triage.getSpringWormTriage().postTriageFinishInterviewWorm();
            }
            else { triage.getSpringWormTriage().postTriageWorm(); }
            Assert.assertEquals(exit.assertDropInQueue(), 1, "Can not verify that patient has entered the drop-in queue due to element missing");
            user.getCancelAppointments().leaveDropInQueue().cancelTheAppointment();
        }

    }
}
