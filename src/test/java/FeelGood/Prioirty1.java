package FeelGood;

import data.Base;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;

import java.io.IOException;

public class Prioirty1 extends Base
{

        public WebDriver driver;


        @BeforeTest
        public void reports() throws IOException
        {
        driver = initializeDriver();
       // log.info("Webbrowser is initialized");
        }

        @Test(dataProvider="getData", dataProviderClass = Priority4.class)
        public void ContactDetails(String ID, String Email, String Phone, String ID2) throws IOException {
                Actions Action = new Actions(driver);
                SoftAssert makeSure = new SoftAssert();
                driver.get(prop.getProperty("Feelgood"));
                LoginAndStartpage User = new LoginAndStartpage(driver);
                User.LoginButton().click();
                User.InputFieldForLogin().sendKeys(ID2);
                User.TestLoginButton().click();
                User.myProfile().click();
                User.enterMyProfileContactDetails().click();
                Action.moveToElement(User.contactDetailsMobileNumberInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
                Action.moveToElement(User.contactDetailsEmailInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
                makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), "");
                makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), "");
                makeSure.assertEquals(User.ConfirmContactDetailsButton().getAttribute("aria-disabled"), "true");
                User.contactDetailsMobileNumberInput().sendKeys(Phone);
                User.contactDetailsEmailInput().sendKeys(Email);
                User.ConfirmContactDetailsButton().click();
                User.enterMyProfileContactDetails().click();
                makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), Phone);
                makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), Email);
                makeSure.assertAll();

        }

                @AfterTest
                public void closeBrowser ()
                {
                        driver.close();
                }


}
