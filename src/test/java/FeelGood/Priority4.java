package FeelGood;

import data.Base;
import data.IDsURL;
import data.pageobjects.ExitsAndPayments.ExitAndPayments;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;
import data.pageobjects.Triage.Triages;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class Priority4 extends Base {

	public WebDriver driver;

	 @BeforeTest
	public void reports() throws IOException
	{
		driver = initializeDriver();
	//	log.info("Webbrowser is initialized");
		
	}



 @Test(dataProvider="InsuranceSSN", dataProviderClass = IDsURL.class)
 public void Feelgood(String ID, String Email, String Phone) throws InterruptedException
 {
	 Triages triage=new Triages(driver);
	 LoginAndStartpage user= new LoginAndStartpage(driver);
	 ExitAndPayments exit= new ExitAndPayments(driver);

	 driver.get(prop.getProperty("Feelgood"));
	 user.loginWithBankID(ID).
	 enterSeekCare().
	 enterSpringWormTriage().
	 seekCareForMyself().click();
	 triage.getSpringWormTriage().SpringWormPrio4();
	 exit.RecommendationSeekCareOnlineButton().click();
	 exit.ChooseToBook().click();
	 exit.PickFirstTimeSlotAvailable().click();
	 exit.ConfirmContactDetailsButton().click();
	 triage.getSpringWormTriage().postTriageFinishInterviewWorm();
	 exit.selectIhaveAFreePass();
	 Assert.assertEquals(exit.assertConfirmBooking(), 1,"Booking details page/button is missing");

	 
	 
 }
	
 @AfterTest
 public void closeBrowser() {
	 driver.close();
 }
 
}
