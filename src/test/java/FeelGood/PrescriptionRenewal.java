package FeelGood;

import data.Base;
import data.IDsURL;
import data.pageobjects.ExitsAndPayments.ExitAndPayments;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;
import data.pageobjects.Triage.Triages;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.io.IOException;

public class PrescriptionRenewal extends Base {
    public WebDriver driver;
    public static Logger log= LogManager.getLogger(PrescriptionRenewal.class.getName());

    @BeforeTest
    public void reports() throws IOException {
        driver = initializeDriver(); }

    @Test(dataProvider = "InsuranceSSN", dataProviderClass = IDsURL.class)
    public void prescriptionRenewal(String ID, String Email, String ID2) throws InterruptedException {
        LoginAndStartpage user = new LoginAndStartpage(driver);
        Triages triage = new Triages(driver);
        ExitAndPayments exit = new ExitAndPayments(driver);
        driver.get(prop.getProperty("Feelgood"));
        user.loginWithBankID(ID2).
        enterPrescriptionRenewal();
        triage.getPrescriptionRenewalTriage().navigateThroughAdultTriage();
        exit.RecommendationSeekCareOnlineButton().click();
        exit.ChooseDropIn().click();
        triage.getPrescriptionRenewalTriage().navigateThroughPostTriageFinishInterview();
        exit.selectIhaveAFreePass();
        Assert.assertEquals(exit.assertDropInQueue(), 1, "Can not verify that patient has entered the drop-in queue due to element missing");

    }
}
