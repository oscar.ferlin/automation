package VasterNorrland;

import data.Base;
import data.IDsURL;
import data.pageobjects.ExitsAndPayments.ExitAndPayments;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;
import data.pageobjects.Triage.Triages;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;

public class Priority4 extends Base {

    @BeforeTest
    public void driver() throws IOException {
        driver = initializeDriver();
    }

    @Test(dataProvider = "GeneralSSN", dataProviderClass = IDsURL.class)
    public void Prio4(String ID, String Email, String Phone) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        LoginAndStartpage user = new LoginAndStartpage(driver);
        ExitAndPayments exit = new ExitAndPayments(driver);
        Triages triage = new Triages(driver);

        driver.get(prop.getProperty("Vasternorrland"));
        //user.loginWithBankID(ID);
        if (driver.getCurrentUrl().contains("/contactDetails")) {
            user.contactDetailsMobileNumberInput().sendKeys(Phone);
            user.contactDetailsEmailInput().sendKeys(Email);
            user.ConfirmContactDetailsButton().click();
        }
        if (user.getCancelAppointments().upcomingAppointmentPopUp() == 1) {
            user.getCancelAppointments().cancelTheAppointment();
        }
        user.enterSeekCare().enterTheCondition("springmask");
        user.seekCareForMyself().click();
        triage.getSpringWormTriage().SpringWormPrio4();
        if (exit.contactElevenSeventySevenBtn() == 1) {
            softAssert.assertEquals(exit.contactElevenSeventySevenButton().getText(), "Ring 1177");
            softAssert.assertTrue(driver.getCurrentUrl().contains("closable=true"));
        } else {
            softAssert.assertEquals(exit.RecommendationSeekCareOnlineButton().getText(), "Chatta med oss");
            exit.RecommendationSeekCareOnlineButton().click();
            int contactForm = driver.findElements(user.contactDetailsEmailInput).size();
            if (contactForm == 1) {
                softAssert.assertEquals(user.ConfirmContactDetailsButton().getText(), "Ja, det stämmer");
                user.ConfirmContactDetailsButton().click();
                exit.waitUntilURLisOfflinePayment();
                exit.AcceptPaymentOfflineButton().click();
            } else {
                exit.waitUntilURLisOfflinePayment();
                exit.AcceptPaymentOfflineButton().click();
                exit.ConfirmContactDetailsButton().click();
            }
            triage.getSpringWormTriage().postTriageWorm();
            Assert.assertEquals(exit.assertDropInQueue(), 1, "Can not verify that patient has entered the drop-in queue due to element missing");
            user.getCancelAppointments().leaveDropInQueue().cancelTheAppointment();
        }
        softAssert.assertAll();
    }

    @Test(dataProvider = "GeneralSSN", dataProviderClass = IDsURL.class)
    public void Prio1(String ID, String Email, String Phone) throws InterruptedException {
        SoftAssert softAssert = new SoftAssert();
        LoginAndStartpage user = new LoginAndStartpage(driver);
        ExitAndPayments exit = new ExitAndPayments(driver);
        Triages triage = new Triages(driver);

        driver.get(prop.getProperty("Vasternorrland"));
        softAssert.assertEquals(user.LoginButton().getText(), "Logga in med BankID");
        user.loginWithBankID(ID);
        if (driver.getCurrentUrl().contains("/contactDetails")) {
            user.contactDetailsMobileNumberInput().sendKeys(Phone);
            user.contactDetailsEmailInput().sendKeys(Email);
            user.ConfirmContactDetailsButton().click();
        }
        if (user.getCancelAppointments().upcomingAppointmentPopUp() == 1) {
            user.getCancelAppointments().cancelTheAppointment();
        }
        user.enterSeekCare().enterTheCondition("handledsskada");
        softAssert.assertEquals(user.seekCareForMyself().getText(), "Mig själv");
        softAssert.assertEquals(user.seekCareForChild().getText(), "Mitt barn under 18");
        user.seekCareForMyself().click();
        triage.getWristInjuryTriage().wristInjuryPrio1();
        if (exit.contactElevenSeventySevenBtn() == 1) {
            softAssert.assertEquals(exit.contactElevenSeventySevenButton().getText(), "Ring 1177");
            softAssert.assertTrue(driver.getCurrentUrl().contains("closable=true"));
        } else {  exit.waitUntilURLisOfflinePayment();
            exit.AcceptPaymentOfflineButton().click();
            exit.ConfirmContactDetailsButton().click();
            Assert.assertEquals(exit.assertDropInQueue(), 1, "Can not verify that patient has entered the drop-in queue due to element missing");
            user.getCancelAppointments().leaveDropInQueue().cancelTheAppointment();
        }
        softAssert.assertAll(); }

}



