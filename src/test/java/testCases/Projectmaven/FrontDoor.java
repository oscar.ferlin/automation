package testCases.Projectmaven;

import data.Base;
import data.IDsURL;
import data.pageobjects.LoginAndHomePage.LoginAndStartpage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import java.io.IOException;


public class FrontDoor extends Base{
	public WebDriver driver;

	
	@BeforeTest
	public void reports() throws IOException{
		driver = initializeDriver();
		//log.info("Webbrowser is initialized");
	}
	
	@Test(dataProvider="GeneralSSN", dataProviderClass = IDsURL.class)
	     public void ContactDetails(String ID, String Email, String Phone) throws IOException {
		 Actions Action =new Actions(driver);
		 SoftAssert makeSure = new SoftAssert();
		 driver.get(prop.getProperty("Norrbotten"));
		 LoginAndStartpage User= new LoginAndStartpage(driver);
		 User.LoginButton().click();
		 User.InputFieldForLogin().sendKeys(ID);
		 User.TestLoginButton().click();
		 User.myProfile().click();
		 User.enterMyProfileContactDetails().click();
		 Action.moveToElement(User.contactDetailsMobileNumberInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
		 Action.moveToElement(User.contactDetailsEmailInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
		 makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), "");
		 makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), "");
		 makeSure.assertEquals(User.ConfirmContactDetailsButton().getAttribute("aria-disabled"), "true");
		 User.contactDetailsMobileNumberInput().sendKeys(Phone);
		 User.contactDetailsEmailInput().sendKeys(Email);
		 User.ConfirmContactDetailsButton().click();
		 User.enterMyProfileContactDetails().click();
		 makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), Phone);
		 makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), Email);
		 makeSure.assertAll();
		 
		 
		
	}
	
	@Test(dataProvider="GeneralSSN", dataProviderClass = IDsURL.class)
	public void Doktor24ContactDetails(String ID, String Email, String Phone) throws IOException, InterruptedException {
		 Actions Action =new Actions(driver);
		 SoftAssert makeSure = new SoftAssert();
		 driver.get(prop.getProperty("URL"));
		 LoginAndStartpage User= new LoginAndStartpage(driver);
		 User.AcceptCookiesDokto24().click();
		 User.LoginDoktor24().click();
		 User.InputFieldForLogin().sendKeys(ID);
		 User.TestLoginButton().click();
		 User.myProfile().click();
		 User.enterMyProfileContactDetails().click();
		 Action.moveToElement(User.contactDetailsMobileNumberInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
		 Action.moveToElement(User.contactDetailsEmailInput()).doubleClick().click().sendKeys(Keys.BACK_SPACE).build().perform();
		 makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), "");
		 makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), "");
		 makeSure.assertEquals(User.ConfirmContactDetailsButton().getAttribute("aria-disabled"), "true");
		 User.contactDetailsMobileNumberInput().sendKeys(Phone);
		 User.contactDetailsEmailInput().sendKeys(Email);
		 User.ConfirmContactDetailsButton().click();
		 User.enterMyProfileContactDetails().click();
		 makeSure.assertEquals(User.contactDetailsMobileNumberInput().getAttribute("value"), Phone);
		 makeSure.assertEquals(User.contactDetailsEmailInput().getAttribute("value"), Email);
		 makeSure.assertAll();
	}
	
	 @Test ( dataProvider="login", dataProviderClass=IDsURL.class)
	 public void SmokeLogin(String ID, String URL) throws InterruptedException, IOException {
		driver.get(URL);
		//log.info("Landed on LoginPage");
		LoginAndStartpage User= new LoginAndStartpage(driver);
		User.LoginButton().click();
		//	log.info("Clicked on Log in with BankID");
		User.InputFieldForLogin().sendKeys(ID);
		User.TestLoginButton().click();
		//log.info("Sucessfully logged in to the patient app");
}
	 @AfterTest
	 public void closeBrowser() {
		 //driver.close();
	 }
}
